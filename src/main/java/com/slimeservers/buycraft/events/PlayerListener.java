package com.slimeservers.buycraft.events;

import com.slimeservers.buycraft.AncientBuycraft;
import com.slimeservers.buycraft.api.obj.OnlineCommandsResponse;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.logging.Level;

public class PlayerListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        try {
            OnlineCommandsResponse response = AncientBuycraft.backend.getOnlineCommandsResponse(event.getPlayer());
            int i = 1;
            for(Object cmd : response.getCommands()) {

                final String command = cmd.toString()
                        .substring(cmd.toString().indexOf("command="))
                        .replace("command=", "")
                        .replace("{name}", event.getPlayer().getName())
                        .replace("{", "")
                        .replace("}", "");
                final String commandID = cmd.toString()
                        .substring(cmd.toString().indexOf("id="))
                        .replace("id=" ,"").split(",")[0];

                Bukkit.getScheduler().runTaskAsynchronously(AncientBuycraft.getInstance(), new BukkitRunnable() {
                    @Override
                    public void run() {
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);

                    }
                });
                AncientBuycraft.backend.deleteCommandID(commandID);
                AncientBuycraft.getInstance().getLogger().log(Level.SEVERE, "Executed Command #" + i + ": " + command);
                i++;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
