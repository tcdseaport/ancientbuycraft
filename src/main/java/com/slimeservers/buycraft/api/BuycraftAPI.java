package com.slimeservers.buycraft.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.slimeservers.buycraft.AncientBuycraft;
import com.slimeservers.buycraft.api.obj.*;
import org.bukkit.entity.Player;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;

public class BuycraftAPI {

    //@see https://docs.tebex.io/plugin/endpoint

    private final URL mainURL, serverInfoURL, commandQueueURL, commandQueueDeleteURL, offlineCommandsURL, onlineCommandsURL, listingURL, packagesURL, packageURL, communityGoalsURL, paymentsURL, checkoutURL, giftCardsURL, couponsURL, bansURL, salesURL, userURL;   ;

    public BuycraftAPI() throws Exception {
            this.mainURL = new URL("https://plugin.tebex.io/");
            this.serverInfoURL = new URL(mainURL + "information/");
            this.commandQueueURL = new URL(mainURL + "queue/");
            this.offlineCommandsURL = new URL(commandQueueURL + "offline-commands/");
            this.onlineCommandsURL = new URL(commandQueueURL + "online-commands/");
            this.listingURL = new URL(mainURL + "listing/");
            this.packagesURL = new URL(mainURL + "packages/");
            this.packageURL = new URL(mainURL + "package/");
            this.communityGoalsURL = new URL(mainURL + "community_goals/");
            this.paymentsURL = new URL(mainURL + "payments/");
            this.checkoutURL = new URL(mainURL + "checkout/");
            this.giftCardsURL = new URL(mainURL + "gift-cards/");
            this.couponsURL = new URL(mainURL + "coupons/");
            this.bansURL = new URL(mainURL + "bans/");
            this.salesURL = new URL(mainURL + "sales/");
            this.userURL = new URL(mainURL + "user/");
            this.commandQueueDeleteURL =  new URL(mainURL + "queue?ids[]=");
    }

    public ServerInfoResponse getServerInfoResponse() throws Exception {
        HttpURLConnection connection = (HttpURLConnection) serverInfoURL.openConnection();
        connection.setRequestProperty("X-Tebex-Secret", AncientBuycraft.instance.secret);
        connection.setRequestMethod("GET");
        connection.connect();
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(inputStreamToString(connection.getInputStream()), ServerInfoResponse.class);
    }

    public CommandQueueResponse getCommandQueueResponse() throws Exception {
        HttpURLConnection connection = (HttpURLConnection)  commandQueueURL.openConnection();
        connection.setRequestProperty("X-Tebex-Secret", AncientBuycraft.instance.secret);
        connection.setRequestMethod("GET");
        connection.connect();
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(inputStreamToString(connection.getInputStream()), CommandQueueResponse.class);
    }

    public String deleteCommandID(String id) throws Exception {
        URL deleteURL = new URL(commandQueueDeleteURL.getProtocol(), commandQueueDeleteURL.getHost(), commandQueueDeleteURL.getPort(), "https://plugin.tebex.io/queue?ids[]=" + id);
        HttpURLConnection connection = (HttpURLConnection) deleteURL.openConnection();
        connection.setRequestProperty("X-Tebex-Secret", AncientBuycraft.instance.secret);
        connection.setRequestMethod("DELETE");
        connection.setDoOutput(true);
        connection.connect();
        ObjectMapper objectMapper = new ObjectMapper();
        return inputStreamToString(connection.getInputStream());
    }


    public UserLookupResponse getUserResponse(String uuid) throws Exception {
        URL usersURL = new URL(userURL.getProtocol(), userURL.getHost(), userURL.getPort(), userURL.getPath() + uuid);
        HttpURLConnection connection = (HttpURLConnection)  usersURL.openConnection();
        connection.setRequestProperty("X-Tebex-Secret", AncientBuycraft.instance.secret);
        connection.setRequestMethod("GET");
        connection.connect();
        JSONObject jsonObject = new JSONObject(inputStreamToString(connection.getInputStream()));
        jsonObject.remove("payments");
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(jsonObject.toString(), UserLookupResponse.class);
    }

    public OnlineCommandsResponse getOnlineCommandsResponse(Player player) throws Exception {
        int userid = getUserResponse(getUUIDOfUsername(player.getName())).getPlayer().getPlugin_username_id();
        URL playerqueue = new URL(userURL.getProtocol(), userURL.getHost(), userURL.getPort(), onlineCommandsURL.getPath() + userid);
        HttpURLConnection connection = (HttpURLConnection)  playerqueue.openConnection();
        connection.setRequestProperty("X-Tebex-Secret", AncientBuycraft.instance.secret);
        connection.setRequestMethod("GET");
        connection.connect();
        JSONObject jsonObject = new JSONObject(inputStreamToString(connection.getInputStream()));
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(jsonObject.toString(), OnlineCommandsResponse.class);
    }

    //TODO: WORKS
    public String getUUIDOfUsername(String username) throws Exception {
        HttpURLConnection connection = (HttpURLConnection)  new URL("https://api.mojang.com/users/profiles/minecraft/" + username).openConnection();
        connection.setRequestProperty("X-Tebex-Secret", AncientBuycraft.instance.secret);
        connection.setRequestMethod("GET");
        connection.connect();
        JSONObject jsonObject = new JSONObject(inputStreamToString(connection.getInputStream()));
        for(String key : jsonObject.keySet()) {
            if(key.equalsIgnoreCase("id")) {
                return jsonObject.getString("id");
            }
        }
        return null;
    }

    private String inputStreamToString(InputStream streamReader) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(streamReader));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line + "/n");
        }
        bufferedReader.close();
        return stringBuilder.toString().replace("/n", "");
    }
}
