package com.slimeservers.buycraft.api.obj;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

public class OnlineCommandsResponse {
    @JsonProperty("commands")
    public  Object[] commands;
    public Player player;

    public Player getPlayer() {
        return player;
    }

    public Object[] getCommands() {
        return commands;
    }

    public class Player {
        public String id;
        public String username;
        public Object meta;

        public Object getMeta() {
            return meta;
        }

        public String getId() {
            return id;
        }

        public String getUsername() {
            return username;
        }
    }
}