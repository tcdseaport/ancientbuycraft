package com.slimeservers.buycraft.api.obj;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class OfflineCommandsResponse {
    public Meta meta;
    public List<Command> commands;

    public Meta getMeta() {
        return meta;
    }

    public List<Command> getCommands() {
        return commands;
    }

    public class Meta {
        public boolean limited;
    }

    public class Command {
        public int id;

        public String command;

        public int payment;

        @JsonProperty("package")
        public int packageName;

        public Conditions conditions;

        public Player player;

        public int getId() {
            return id;
        }

        public String getCommand() {
            return command;
        }

        public int getPayment() {
            return payment;
        }

        public Conditions getConditions() {
            return conditions;
        }

        public Player getPlayer() {
            return player;
        }

        public class Player {
            public int id;
            public String name;
            public String uuid;

            public int getId() {
                return id;
            }

            public String getName() {
                return name;
            }

            public String getUuid() {
                return uuid;
            }
        }

        public class Conditions {
            public int delay;

            public int getDelay() {
                return delay;
            }
        }
    }
}
