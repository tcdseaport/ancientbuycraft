package com.slimeservers.buycraft.api.obj;

import java.util.List;

public class CommandQueueResponse {
    public Meta meta;
    public List<Player> players;

    public Meta getMeta() {
        return meta;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public class Player {
        public int id;
        public String name;
        public String uuid;

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getUuid() {
            return uuid;
        }
    }

    public class Meta {
        public boolean execute_offline;
        public int next_check;
        public boolean more;

        public boolean execute_offline() {
            return execute_offline;
        }

        public int getNext_check() {
            return next_check;
        }

        public boolean isMore() {
            return more;
        }
    }
}
