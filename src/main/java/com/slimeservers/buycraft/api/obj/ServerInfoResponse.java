package com.slimeservers.buycraft.api.obj;

public class ServerInfoResponse {
    public Account account;
    public Server server;
    public Analytics analytics;

    public Server getServer() {
        return server;
    }

    public Account getAccount() {
        return account;
    }

    public Analytics getAnalytics() {
        return analytics;
    }

    public class Account{
        public int id;
        public String domain;
        public String name;
        public Currency currency;
        public boolean online_mode;
        public String game_type;
        public boolean log_events;

        public int getId() {
            return id;
        }

        public String getDomain() {
            return domain;
        }

        public String getName() {
            return name;
        }

        public Currency getCurrency() {
            return currency;
        }

        public boolean isOnline_mode() {
            return online_mode;
        }

        public String getGame_type() {
            return game_type;
        }

        public boolean log_events() {
            return log_events;
        }

        public class Currency{
            String iso_4217;
            String symbol;

            public String getSymbol() {
                return symbol;
            }

            public String getIso_4217() {
                return iso_4217;
            }
        }
    }

    public class Server{
        public int id;
        public String name;

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

    public class Analytics{
        public Internal internal;

        public Internal getInternal() {
            return internal;
        }

        public class Internal {
            String project, key;

            public String getProject() {
                return project;
            }

            public String getKey() {
                return key;
            }
        }
    }
}
