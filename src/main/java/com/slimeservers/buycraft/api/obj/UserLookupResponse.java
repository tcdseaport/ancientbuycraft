package com.slimeservers.buycraft.api.obj;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class UserLookupResponse {
    private Player player;
    private int banCount;
    private int chargebackRate;
    private PurchaseTotals purchaseTotals;

    public void setPlayer(Player player){
        this.player = player;
    }
    public Player getPlayer(){
        return this.player;
    }
    public void setBanCount(int banCount){
        this.banCount = banCount;
    }
    public int getBanCount(){
        return this.banCount;
    }
    public void setChargebackRate(int chargebackRate){
        this.chargebackRate = chargebackRate;
    }
    public int getChargebackRate(){
        return this.chargebackRate;
    }

    public void setPurchaseTotals(PurchaseTotals purchaseTotals){
        this.purchaseTotals = purchaseTotals;
    }
    public PurchaseTotals getPurchaseTotals(){
        return this.purchaseTotals;
    }

    public class Player
    {
        private String id;

        private String created_at;

        private String updated_at;

        private String cache_expire;

        private String username;

        private String meta;

        private int plugin_username_id;

        public void setId(String id){
            this.id = id;
        }
        public String getId(){
            return this.id;
        }
        public void setCreated_at(String created_at){
            this.created_at = created_at;
        }
        public Object getCreated_at(){
            return this.created_at;
        }
        public void setUpdated_at(String updated_at){
            this.updated_at = updated_at;
        }
        public Object getUpdated_at(){
            return this.updated_at;
        }
        public void setCache_expire(String cache_expire){
            this.cache_expire = cache_expire;
        }
        public String getCache_expire(){
            return this.cache_expire;
        }
        public void setUsername(String username){
            this.username = username;
        }
        public String getUsername(){
            return this.username;
        }
        public void setMeta(String meta){
            this.meta = meta;
        }
        public String getMeta(){
            return this.meta;
        }
        public void setPlugin_username_id(int plugin_username_id){
            this.plugin_username_id = plugin_username_id;
        }
        public int getPlugin_username_id(){
            return this.plugin_username_id;
        }
    }

    public class PurchaseTotals
    {
        @JsonProperty("USD")
        private int USD;

        public void setUSD(int USD){
            this.USD = USD;
        }
        public int getUSD(){
            return this.USD;
        }
    }
}
