package com.slimeservers.buycraft.commands;

import com.slimeservers.buycraft.AncientBuycraft;
import com.slimeservers.buycraft.api.obj.OnlineCommandsResponse;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.logging.Level;


public class BuycraftFetchCommand implements CommandExecutor {

    public BuycraftFetchCommand(AncientBuycraft ancientBuycraft) {
    }

    public boolean onCommand(CommandSender sender, Command command, String Label, String[] args) {
        if (sender.hasPermission("buycraft.buycraftfetch")) {
            try {
                for (Player online : Bukkit.getOnlinePlayers()) {
                    OnlineCommandsResponse response = AncientBuycraft.backend.getOnlineCommandsResponse(online);
                    int i = 1;
                    for (Object cmd : response.getCommands()) {
                        final String cmd2 = cmd.toString()
                                .substring(cmd.toString().indexOf("command="))
                                .replace("command=", "")
                                .replace("{name}", online.getName())
                                .replace("{", "")
                                .replace("}", "");
                        final String commandID = cmd.toString()
                                .substring(cmd.toString().indexOf("id="))
                                .replace("id=", "").split(",")[0];

                        Bukkit.getScheduler().runTaskAsynchronously(AncientBuycraft.getInstance(), new BukkitRunnable() {
                            @Override
                            public void run() {
                                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd2);
                            }
                        });

                        AncientBuycraft.getInstance().getLogger().log(Level.INFO, "DELETED: " + AncientBuycraft.backend.deleteCommandID(commandID));
                        AncientBuycraft.getInstance().getLogger().log(Level.SEVERE, "Executed Command #" + i + ": " + cmd2);
                        i++;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            sender.sendMessage(ChatColor.RED + "No Permission!");
            return true;
        }
        return true;
    }
}