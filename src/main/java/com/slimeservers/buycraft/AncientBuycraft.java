package com.slimeservers.buycraft;

import com.slimeservers.buycraft.api.BuycraftAPI;
import com.slimeservers.buycraft.commands.BuyCommand;
import com.slimeservers.buycraft.commands.BuycraftCommand;
import com.slimeservers.buycraft.commands.BuycraftFetchCommand;
import com.slimeservers.buycraft.events.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;


public class AncientBuycraft extends JavaPlugin {

    public static AncientBuycraft instance;

    private FileConfiguration config;
    public String secret = "secret";
    public static int tid = 0;
    public static BuycraftAPI backend;

    @Override
    public void onEnable() {
        if (secret == null) {
            getServer().getPluginManager().disablePlugin(this);
            getLogger().info("Buycraft disabled due to invalid configuration. (set the secret)");
        }
        fetchSecret();
        instance = this;
        loadListener(new PlayerListener());
        try {
            backend = new BuycraftAPI();
        } catch(Exception e) {
            e.printStackTrace();
        }

        loadConfiguration();
        registercommands();
        getLogger().info("Buycraft loaded.");
    }

    @Override
    public void onDisable() {
        getLogger().info("Buycraft disabling.");
    }

    public final void loadListener(Listener lis) {
        getServer().getPluginManager().registerEvents(lis, this);
    }

    public void registercommands() {
        this.getCommand("buycraft").setExecutor( new BuycraftCommand(this));
        this.getCommand("buy").setExecutor( new BuyCommand(this));
        this.getCommand("buycraftfetch").setExecutor( new BuycraftFetchCommand(this));
    }

    public void loadConfiguration() {
        this.config = getConfig();
        getConfig().options().copyDefaults(true);
        saveConfig();
    }

    public void fetchSecret(){
        this.secret = getConfig().getString("secret").equals("nill") ? null : getConfig().getString("secret");
    }

    public static AncientBuycraft getInstance() {
        return instance;
    }
}
